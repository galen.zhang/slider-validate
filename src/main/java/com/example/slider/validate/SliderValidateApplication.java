package com.example.slider.validate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SliderValidateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SliderValidateApplication.class, args);
	}

}
