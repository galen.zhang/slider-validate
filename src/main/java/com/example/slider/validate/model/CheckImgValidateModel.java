package com.example.slider.validate.model;

public class CheckImgValidateModel {
    private int offsetHorizontal;
    private String telephone;

    public int getOffsetHorizontal() {
        return offsetHorizontal;
    }

    public void setOffsetHorizontal(int offsetHorizontal) {
        this.offsetHorizontal = offsetHorizontal;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
